import tickersReducer from '../reducers/tickersReducer'
import {
  periodicRefresh,
  getCurrenciesAndTickers
} from '../actions/tickerActions'
import { createStore, applyMiddleware, compose } from 'redux'
import thunkMiddleware from 'redux-thunk'
import persistState from 'redux-localstorage'

export default () => {
  const store = createStore(
    tickersReducer,
    populateState({}),
    compose(persistState(), applyMiddleware(thunkMiddleware))
  )
  const getSelectedTickers = () =>
    store
      .getState()
      .selectedTickers.map(tickerId =>
        store.getState().tickers.find(ticker => ticker.id === tickerId)
      )
  if (store.getState().tickers.length === 0) {
    store.dispatch(getCurrenciesAndTickers())
    store.dispatch(periodicRefresh(getSelectedTickers))
  } else {
    store.dispatch(periodicRefresh(getSelectedTickers))
  }
  return store
}

function populateState(state) {
  const stateCopy = { ...state }
  if (!stateCopy.currencies) {
    stateCopy.currencies = []
  }
  if (!stateCopy.tickers) {
    stateCopy.tickers = []
  }
  if (!stateCopy.selectedTickers) {
    stateCopy.selectedTickers = []
  }
  return stateCopy
}
