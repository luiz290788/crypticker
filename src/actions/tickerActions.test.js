import { expect } from 'chai';
import * as actionTypes from './actionTypes';

import * as actions from './tickerActions';

global.fetch = jest.fn();
jest.useFakeTimers();

const tickerActionBuilderTest = (actionCreator, type) => () => {
  const ticker = {
    id: 'BTCEUR',
    currFrom: 'BTC',
    currTo: 'EUR',
    value: 950.0,
  };
  expect(actionCreator(ticker)).to.deep.equal({
    type,
    ticker,
  });
};

const onlyActionTest = (actionCreator, type) => () => {
  expect(actionCreator()).to.deep.equal({ type });
};

const fetchResponseMock = body =>
  new Promise(resolve => resolve({ json: () => new Promise(resolve => resolve(body)) }));

const selectedTickers = [
  {
    id: 'BTCEUR',
    currFrom: 'BTC',
    currTo: 'EUR',
    value: 850.0,
  },
];

beforeEach(() => jest.resetAllMocks());

it('should build remove ticker action', tickerActionBuilderTest(actions.removeTicker, actionTypes.REMOVE_TICKER));

it('should build move up action', tickerActionBuilderTest(actions.moveUpTicker, actionTypes.MOVE_UP_TICKER));

it('should build move down action', tickerActionBuilderTest(actions.moveDownTicker, actionTypes.MOVE_DOWN_TICKER));

it('should build activate edit mode action', onlyActionTest(actions.activateEditMode, actionTypes.ACTIVATE_EDIT_MODE));

it(
  'should build deactivate edit mode action',
  onlyActionTest(actions.deactivateEditMode, actionTypes.DEACTIVATE_EDIT_MODE),
);

it(
  'should build select ticker to add action',
  tickerActionBuilderTest(actions.selectTickerToAdd, actionTypes.SELECT_TICKER_TO_ADD),
);

it('should build add ticker action', onlyActionTest(actions.addTicker, actionTypes.ADD_TICKER));

it('should deactivate edit mode and refresh', () => {
  const dispatch = jest.fn();
  actions.deactivateEditModeAndRefresh(selectedTickers)(dispatch);
  expect(dispatch.mock.calls[0][0]).to.deep.equal(actions.deactivateEditMode());
  setTimeout.mock.calls[0][0](); // for some reason jest.runAllTimers(); is not working
  expect(dispatch.mock.calls.length).to.be.equal(2);
});

it('should refresh', () => {
  const dispatch = jest.fn();
  fetch.mockReturnValueOnce(
    fetchResponseMock([
      {
        id: 'BTCEUR',
        currFrom: 'BTC',
        currTo: 'EUR',
        value: 950.0,
      },
    ]),
  );
  actions.refresh(selectedTickers)(dispatch).then(() => {
    expect(dispatch.mock.calls[0][0]).to.deep.equal({
      type: actionTypes.REQUEST_TICKERS,
    });
    expect(dispatch.mock.calls[1][0].tickers).to.deep.equal([
      {
        id: 'BTCEUR',
        currFrom: 'BTC',
        currTo: 'EUR',
        value: 950.0,
      },
    ]);
  });
});

it('should show error if error happens while refreshing', () => {
  fetch.mockReturnValueOnce(new Promise((resolve, reject) => reject()));

  const dispatch = jest.fn();
  return actions.refresh(selectedTickers)(dispatch).then(() => {
    expect(dispatch.mock.calls[0][0]).to.deep.equal({
      type: actionTypes.REQUEST_TICKERS,
    });
    dispatch.mock.calls[1][0](dispatch);
    expect(dispatch.mock.calls[2][0].tickers).to.deep.equal([]);
    expect(dispatch.mock.calls[3][0]).to.deep.equal(actions.showError('Error while fetching tickers.'));
    setTimeout.mock.calls[0][0](); // for some reason jest.runAllTimers(); is not working
    expect(dispatch.mock.calls[4][0]).to.deep.equal(actions.hideError());
  });
});

it('should build open about dialog action', onlyActionTest(actions.openAboutDialog, actionTypes.OPEN_ABOUT_DIALOG));

it('should build close about dialog action', onlyActionTest(actions.closeAboutDialog, actionTypes.CLOSE_ABOUT_DIALOG));

it('should build hide error action', onlyActionTest(actions.hideError, actionTypes.HIDE_ERROR));

it('should build show error action', () => {
  expect(actions.showError('error message')).to.deep.equal({
    type: actionTypes.SHOW_ERROR,
    errorMessage: 'error message',
  });
});
