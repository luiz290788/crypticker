import * as actionTypes from '../actions/actionTypes'
import tickersReducer from './tickersReducer'
import { expect } from 'chai'

const tickers = [
  {
    id: 'BTCEUR',
    currFrom: 'BTC',
    currTo: 'EUR',
    value: 950.0
  },
  {
    id: 'BTCUSD',
    currFrom: 'BTC',
    currTo: 'USD',
    value: 1000.0
  },
  {
    id: 'ETHBTC',
    currFrom: 'ETH',
    currTo: 'BTC',
    value: 0.00115
  },
  {
    id: 'ETHEUR',
    currFrom: 'ETH',
    currTo: 'EUR',
    value: 10.5
  },
  {
    id: 'ETHUSD',
    currFrom: 'ETH',
    currTo: 'USD',
    value: 11
  }
]
function getTicker(tickerId) {
  return tickers.find(ticker => ticker.id === tickerId)
}

it('should remove ticker', () => {
  const state = { tickers, selectedTickers: ['BTCEUR', 'BTCUSD', 'ETHEUR'] }
  const action = {
    type: actionTypes.REMOVE_TICKER,
    ticker: getTicker('BTCUSD')
  }
  const newState = tickersReducer(state, action)
  expect(newState).to.deep.equal({
    tickers,
    selectedTickers: ['BTCEUR', 'ETHEUR']
  })
})

it('should move up a ticker', () => {
  const state = { tickers, selectedTickers: ['BTCEUR', 'BTCUSD', 'ETHEUR'] }
  const action = {
    type: actionTypes.MOVE_UP_TICKER,
    ticker: getTicker('BTCUSD')
  }
  const newState = tickersReducer(state, action)
  expect(newState).to.deep.equal({
    tickers,
    selectedTickers: ['BTCUSD', 'BTCEUR', 'ETHEUR']
  })
})

it('should not do anything if trying to move up the top ticker', () => {
  const state = { tickers, selectedTickers: ['BTCEUR', 'BTCUSD', 'ETHEUR'] }
  const action = {
    type: actionTypes.MOVE_UP_TICKER,
    ticker: getTicker('BTCEUR')
  }
  const newState = tickersReducer(state, action)
  expect(newState).to.deep.equal({ ...state })
})

it('should move down a ticker', () => {
  const state = { tickers, selectedTickers: ['BTCEUR', 'BTCUSD', 'ETHEUR'] }
  const action = {
    type: actionTypes.MOVE_DOWN_TICKER,
    ticker: getTicker('BTCUSD')
  }
  const newState = tickersReducer(state, action)
  expect(newState).to.deep.equal({
    tickers,
    selectedTickers: ['BTCEUR', 'ETHEUR', 'BTCUSD']
  })
})

it('should not do anything if trying to move down the bottom ticker', () => {
  const state = { tickers, selectedTickers: ['BTCEUR', 'BTCUSD', 'ETHEUR'] }
  const action = {
    type: actionTypes.MOVE_DOWN_TICKER,
    ticker: getTicker('ETHEUR')
  }
  const newState = tickersReducer(state, action)
  expect(newState).to.deep.equal({ ...state })
})

it('should activate edit mode', () => {
  const state = { editMode: false }
  const action = { type: actionTypes.ACTIVATE_EDIT_MODE }
  const newState = tickersReducer(state, action)
  expect(newState).to.deep.equal({ editMode: true })
})

it('should deactivate edit mode', () => {
  const state = { editMode: true }
  const action = { type: actionTypes.DEACTIVATE_EDIT_MODE }
  const newState = tickersReducer(state, action)
  expect(newState).to.deep.equal({ editMode: false })
})

it('should select ticker to add', () => {
  const state = {}
  const action = {
    type: actionTypes.SELECT_TICKER_TO_ADD,
    ticker: getTicker('BTCEUR')
  }
  const newState = tickersReducer(state, action)
  expect(newState).to.deep.equal({ selectedTickerToAdd: 'BTCEUR' })
})

it('should add a ticker', () => {
  const state = { tickers, selectedTickerToAdd: 'BTCEUR', selectedTickers: [] }
  const action = { type: actionTypes.ADD_TICKER }
  const newState = tickersReducer(state, action)
  expect(newState).to.deep.equal({
    tickers,
    selectedTickers: ['BTCEUR'],
    selectedTickerToAdd: 'BTCUSD'
  })
})

it('should not do anything when trying to add a ticker if no ticker selected', () => {
  const state = {
    tickers,
    selectedTickerToAdd: null,
    selectedTickers: tickers.map(t => t.id)
  }
  const action = { type: actionTypes.ADD_TICKER }
  const newState = tickersReducer(state, action)
  expect(newState).to.deep.equal({ ...state })
})

it('should not change state if unknown action', () => {
  const state = { tickers }
  const action = { type: 'unknown action' }
  const newState = tickersReducer(state, action)
  expect(newState).to.deep.equal(state)
})

it('should set requestingTickers to true when refreshing tickers', () => {
  const state = {}
  const action = { type: actionTypes.REQUEST_TICKERS }
  const newState = tickersReducer(state, action)
  expect(newState).to.deep.equal({ requestingTickers: true })
})

it('should set requestingTickers to false and have tickers when tickers are fetched', () => {
  const state = { requestingTickers: true }
  const action = { type: actionTypes.RECEIVE_TICKERS }
  const newState = tickersReducer(state, action)
  expect(newState.requestingTickers).to.be.false
  expect(newState.tickers).to.be.an.Array
  expect(newState.lastUpdated).to.be.an.String
})

it('should set aboutDialogOpen to true when opening about dialog', () => {
  const state = {}
  const action = { type: actionTypes.OPEN_ABOUT_DIALOG }
  const newState = tickersReducer(state, action)
  expect(newState.aboutDialogOpen).to.be.true
})

it('should set aboutDialogOpen to false when closing about dialog', () => {
  const state = { aboutDialogOpen: true }
  const action = { type: actionTypes.CLOSE_ABOUT_DIALOG }
  const newState = tickersReducer(state, action)
  expect(newState.aboutDialogOpen).to.be.false
})

it('should set errorDisplayed to true and errorMessage when showing an error', () => {
  const state = {}
  const action = {
    type: actionTypes.SHOW_ERROR,
    errorMessage: 'Error happened'
  }
  const newState = tickersReducer(state, action)
  expect(newState.errorDisplayed).to.be.true
  expect(newState.errorMessage).to.equal(action.errorMessage)
})

it('should reset errorDisplayed and errorMessage when hiding error', () => {
  const state = {}
  const action = {
    type: actionTypes.HIDE_ERROR
  }
  const newState = tickersReducer(state, action)
  expect(newState.errorDisplayed).to.be.false
  expect(newState.errorMessage).to.be.null
})
