import * as actionTypes from '../actions/actionTypes'

export default function tickersReducer(state, action) {
  switch (action.type) {
    case actionTypes.REMOVE_TICKER: {
      return {
        ...state,
        selectedTickers: state.selectedTickers.filter(
          tickerId => tickerId !== action.ticker.id
        )
      }
    }
    case actionTypes.MOVE_UP_TICKER: {
      const index = state.selectedTickers.indexOf(action.ticker.id)
      return {
        ...state,
        selectedTickers: switchItemsInArray(
          state.selectedTickers,
          index,
          index - 1
        )
      }
    }
    case actionTypes.MOVE_DOWN_TICKER: {
      const index = state.selectedTickers.indexOf(action.ticker.id)
      return {
        ...state,
        selectedTickers: switchItemsInArray(
          state.selectedTickers,
          index,
          index + 1
        )
      }
    }
    case actionTypes.ACTIVATE_EDIT_MODE: {
      return { ...state, editMode: true }
    }
    case actionTypes.DEACTIVATE_EDIT_MODE: {
      return { ...state, editMode: false }
    }
    case actionTypes.SELECT_TICKER_TO_ADD: {
      return { ...state, selectedTickerToAdd: action.ticker.id }
    }
    case actionTypes.ADD_TICKER: {
      const selectedTickers = state.selectedTickerToAdd
        ? [...state.selectedTickers, state.selectedTickerToAdd]
        : state.selectedTickers
      const selectedTickerToAdd = state.tickers.find(
        ticker => !selectedTickers.includes(ticker.id)
      )
      return {
        ...state,
        selectedTickers,
        selectedTickerToAdd: selectedTickerToAdd ? selectedTickerToAdd.id : null
      }
    }
    case actionTypes.ADD_TICKERS: {
      return {
        ...state,
        selectedTickers: action.tickersIds
      }
    }
    case actionTypes.REQUEST_TICKERS: {
      return {
        ...state,
        requestingTickers: true
      }
    }
    case actionTypes.RECEIVE_TICKERS: {
      return {
        ...state,
        lastUpdated: action.date,
        requestingTickers: false,
        tickers: state.tickers
          ? state.tickers.map(ticker => {
              const newTicker = action.tickers.find(t => t.id === ticker.id)
              return newTicker || ticker
            })
          : []
      }
    }
    case actionTypes.OPEN_ABOUT_DIALOG: {
      return {
        ...state,
        aboutDialogOpen: true
      }
    }
    case actionTypes.CLOSE_ABOUT_DIALOG: {
      return {
        ...state,
        aboutDialogOpen: false
      }
    }
    case actionTypes.REQUEST_CURRENCIES_AND_AVAILABLE_TICKERS: {
      return {
        ...state,
        requestingCurrenciesAndAvailableTickers: true
      }
    }
    case actionTypes.RECEIVE_CURRENCIES_AND_AVAILABLE_TICKERS: {
      const newState = {
        ...state,
        requestingCurrenciesAndAvailableTickers: false,
        currencies: action.currencies,
        tickers: action.tickers,
        selectedTickers: state.selectedTickers.filter(tickerId =>
          action.tickers.some(ticker => ticker.id === tickerId)
        ),
        selectedTickerToAdd: action.tickers.includes(state.selectedTickerToAdd)
          ? state.selectedTickerToAdd
          : null
      }

      return newState
    }
    case actionTypes.SHOW_ERROR: {
      return {
        ...state,
        errorDisplayed: true,
        errorMessage: action.errorMessage
      }
    }
    case actionTypes.HIDE_ERROR: {
      return {
        ...state,
        errorDisplayed: false,
        errorMessage: null
      }
    }
    default:
      return { ...state }
  }
}

function switchItemsInArray(array, index1, index2) {
  const arrayCopy = [...array]
  if (
    index1 >= 0 &&
    index1 < array.length &&
    index2 >= 0 &&
    index2 < array.length
  ) {
    ;[arrayCopy[index1], arrayCopy[index2]] = [
      arrayCopy[index2],
      arrayCopy[index1]
    ]
  }
  return arrayCopy
}
