import React, { Component } from 'react'
import PropTypes from 'prop-types'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'

import './TickerSelector.css'

export default class TickerSelector extends Component {
  static propTypes = {
    tickers: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        currFrom: PropTypes.string.isRequired,
        currTo: PropTypes.string.isRequired
      })
    ).isRequired,
    currencies: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        symbol: PropTypes.string.isRequired
      })
    ).isRequired,
    selectedTicker: PropTypes.shape({
      id: PropTypes.string.isRequired,
      currFrom: PropTypes.string.isRequired,
      currTo: PropTypes.string.isRequired
    }),
    selectTicker: PropTypes.func
  }
  getOptionLabelForTicker (ticker) {
    return `${this.getCurrency(ticker.currFrom).symbol} → ${this.getCurrency(
      ticker.currTo
    ).symbol}`
  }
  getCurrency (currencyId) {
    return this.props.currencies.find(currency => currency.id === currencyId)
  }
  getTicker (tickerId) {
    return this.props.tickers.find(ticker => ticker.id === tickerId)
  }
  onSelectedTickerChanged (value) {
    if (this.props.selectTicker) {
      this.props.selectTicker(this.getTicker(value))
    }
  }
  render () {
    return (
      <SelectField
        disabled={this.props.tickers.length === 0}
        className='ticker-selector'
        hintText='Select a ticker'
        fullWidth={true}
        onChange={(event, index, value) => this.onSelectedTickerChanged(value)}
        value={this.props.selectedTicker && this.props.selectedTicker.id}
      >
        {this.props.tickers.map(ticker =>
          <MenuItem
            className='ticker-selector-item'
            key={ticker.id}
            value={ticker.id}
            primaryText={this.getOptionLabelForTicker(ticker)}
          />
        )}
      </SelectField>
    )
  }
}
