import React from 'react'
import ReactDOM from 'react-dom'
import ReactTestUtils from 'react-dom/test-utils'
import { expect } from 'chai'
import TickerSelector from './TickerSelector'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import injectTapEventPlugin from 'react-tap-event-plugin'
// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin()

const tickers = [
  {
    id: 'BTCEUR',
    currFrom: 'BTC',
    currTo: 'EUR',
    value: 950.0
  },
  {
    id: 'BTCUSD',
    currFrom: 'BTC',
    currTo: 'USD',
    value: 1000.0
  }
]
const currencies = [
  { id: 'BTC', symbol: 'Ƀ' },
  { id: 'EUR', symbol: '€' },
  { id: 'USD', symbol: '$' }
]
const getCurrency = currencyId =>
  currencies.find(currency => currency.id === currencyId)
const getExpectedTextForTicker = ticker =>
  getCurrency(ticker.currFrom).symbol +
  ' → ' +
  getCurrency(ticker.currTo).symbol

it('should list available tickers', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <MuiThemeProvider>
      <TickerSelector
        tickers={tickers}
        currencies={currencies}
        selectedTicker={tickers[1]}
      />
    </MuiThemeProvider>,
    div
  )

  const selectElement = div.querySelector('.ticker-selector')
  expect(selectElement).to.exist
  expect(selectElement.innerHTML).to.contain(
    getExpectedTextForTicker(tickers[1])
  )
  expect(selectElement.hasAttribute('disabled')).to.be.false
  ReactTestUtils.Simulate.touchTap(
    selectElement.querySelector('div:nth-child(2) > div')
  )
  const menuElement = document.querySelector('[role=menu]')
  expect(menuElement).to.exist
  const optionElements = menuElement.querySelectorAll(
    '[role="menuitem"] > div > div > div'
  )
  expect(Array.from(optionElements).map(e => e.innerHTML)).to.deep.equal([
    'Ƀ → €',
    'Ƀ → $'
  ])
})

it('should be disabled if no available ticker', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <MuiThemeProvider>
      <TickerSelector tickers={[]} currencies={currencies} />
    </MuiThemeProvider>,
    div
  )

  const selectElement = div.querySelector('.ticker-selector')
  expect(selectElement).to.exist
  // expect(selectElement.hasAttribute('disabled')).to.be.true
  // const optionElements = selectElement.querySelectorAll('option')
  // expect(optionElements).have.lengthOf(0)
})

it('should call selectTicker when a ticker is selected', () => {
  const selectTicker = jest.fn()
  const div = document.createElement('div')
  ReactDOM.render(
    <MuiThemeProvider>
      <TickerSelector
        tickers={tickers}
        currencies={currencies}
        selectedTicker={tickers[0]}
        selectTicker={selectTicker}
      />
    </MuiThemeProvider>,
    div
  )

  const selectElement = div.querySelector('.ticker-selector')
  expect(selectElement).to.exist
  expect(selectElement.hasAttribute('disabled')).to.be.false

  ReactTestUtils.Simulate.touchTap(
    selectElement.querySelector('div:nth-child(2) > div')
  )
  const menuElement = document.querySelector('[role=menu]')
  expect(menuElement).to.exist
  const optionElements = menuElement.querySelectorAll('[role="menuitem"]')
  ReactTestUtils.Simulate.touchTap(optionElements[0])
  // const optionElements = selectElement.querySelectorAll('option')
  //
  // ReactTestUtils.Simulate.change(selectElement, { value: tickers[1].id })
  //
  // expect(selectTicker.mock.calls).to.have.lengthOf(1)
  // expect(selectTicker.mock.calls[0][0]).to.deep.equal(tickers[1])
})

it('should not crash if no selectTicker passed', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <MuiThemeProvider>
      <TickerSelector
        tickers={tickers}
        currencies={currencies}
        selectedTicker={tickers[0]}
      />
    </MuiThemeProvider>,
    div
  )

  const selectElement = div.querySelector('.ticker-selector')
  expect(selectElement).to.exist
  expect(selectElement.hasAttribute('disabled')).to.be.false
  const optionElements = selectElement.querySelectorAll('option')

  ReactTestUtils.Simulate.change(selectElement)
})
