import React from 'react'
import ReactDOM from 'react-dom'
import ReactTestUtils from 'react-dom/test-utils'
import { expect } from 'chai'
import AboutDialog from './AboutDialog'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import injectTapEventPlugin from 'react-tap-event-plugin'
// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin()

it('should render and close modal when clicking close button', () => {
  const div = document.createElement('div')
  const closeAboutDialog = jest.fn()
  ReactDOM.render(
    <MuiThemeProvider>
      <AboutDialog closeAboutDialog={closeAboutDialog} />
    </MuiThemeProvider>,
    div
  )

  const closeButton = document.querySelector('.close-button')
  expect(closeButton).to.exist
  ReactTestUtils.Simulate.touchTap(closeButton)
  expect(closeAboutDialog.mock.calls).to.be.lengthOf(1)
})
