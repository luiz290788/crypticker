import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Card, CardActions } from 'material-ui/Card'
import FlatButton from 'material-ui/FlatButton'

import './ListView.css'

const cardContainerStyle = {
  display: 'flex',
  flexWrap: 'wrap',
  marginTop: '12px',
  justifyContent: 'center'
}

const cardsStyle = {
  flexGrow: 1,
  flexShrink: 0,
  margin: '0 10px 12px',
  width: '100wh',
  minWidth: '300px',
  maxWidth: '400px'
}

export default class ListView extends Component {
  static propTypes = {
    items: PropTypes.arrayOf(PropTypes.node).isRequired,
    editMode: PropTypes.bool,
    removeItem: PropTypes.func,
    moveUpItem: PropTypes.func,
    moveDownItem: PropTypes.func
  }
  onRemoveButtonClicked(event, index) {
    event.preventDefault()
    this.props.removeItem && this.props.removeItem(index)
  }
  onMoveUpButtonClicked(event, index) {
    event.preventDefault()
    this.props.moveUpItem && this.props.moveUpItem(index)
  }
  onMoveDownButtonClicked(event, index) {
    event.preventDefault()
    this.props.moveDownItem && this.props.moveDownItem(index)
  }
  render() {
    return (
      <div style={cardContainerStyle}>
        {this.props.items.map((item, index) =>
          <Card className="item" key={index} style={cardsStyle}>
            <div className="item-content">
              {item}
            </div>
            {this.props.editMode &&
              <CardActions
                className="action-buttons"
                style={{ textAlign: 'right' }}
              >
                {index > 0 &&
                  <FlatButton
                    className="move-up-button"
                    label="Move up"
                    primary={true}
                    onClick={e => this.onMoveUpButtonClicked(e, index)}
                  />}
                {index < this.props.items.length - 1 &&
                  <FlatButton
                    className="move-down-button"
                    label="Move down"
                    primary={true}
                    onClick={e => this.onMoveDownButtonClicked(e, index)}
                  />}
                <FlatButton
                  className="remove-button"
                  label="Remove"
                  secondary={true}
                  onClick={e => this.onRemoveButtonClicked(e, index)}
                />
              </CardActions>}
          </Card>
        )}
      </div>
    )
  }
}
