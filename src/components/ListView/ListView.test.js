import React from 'react'
import ReactDOM from 'react-dom'
import { expect } from 'chai'
import ReactTestUtils from 'react-dom/test-utils'
import ListView from './ListView'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import injectTapEventPlugin from 'react-tap-event-plugin'
// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin()

const itemContents = ['item #1', 'item #2', 'item #3']
const items = itemContents.map(content => <div>{content}</div>)

it('should display list of elements', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <MuiThemeProvider>
      <ListView items={items} />
    </MuiThemeProvider>,
    div
  )
  const itemElements = div.querySelectorAll('.item')
  expect(
    Array.from(itemElements).map(
      e => e.querySelector('.item-content').firstChild.innerHTML
    )
  ).to.deep.equal(itemContents)
})

it('should display action buttons if edit mode is on', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <MuiThemeProvider>
      <ListView items={items} editMode={true} />
    </MuiThemeProvider>,
    div
  )
  const itemElements = div.querySelectorAll('.item')
  itemElements.forEach((itemElement, itemIndex) => {
    const actionButtonsDiv = itemElement.querySelector('.action-buttons')
    expect(actionButtonsDiv).to.exist
    expect(actionButtonsDiv.querySelector('.remove-button')).to.exist
    const moveUpButton = actionButtonsDiv.querySelector('.move-up-button')
    if (itemIndex > 0) {
      expect(moveUpButton).to.exist
    } else {
      expect(moveUpButton).not.to.exist
    }
    const moveDownButton = actionButtonsDiv.querySelector('.move-down-button')
    if (itemIndex < itemElements.length - 1) {
      expect(moveDownButton).to.exist
    } else {
      expect(moveDownButton).not.to.exist
    }
  })
})

it('should respond to clicks on action buttons', () => {
  const removeItem = jest.fn()
  const moveUpItem = jest.fn()
  const moveDownItem = jest.fn()
  const div = document.createElement('div')
  ReactDOM.render(
    <MuiThemeProvider>
      <ListView
        items={items}
        editMode={true}
        removeItem={removeItem}
        moveUpItem={moveUpItem}
        moveDownItem={moveDownItem}
      />
    </MuiThemeProvider>,
    div
  )
  const actionButtonsElement = div.querySelector(
    '.item:nth-of-type(2) .action-buttons'
  )

  const removeButton = actionButtonsElement.querySelector('.remove-button')
  ReactTestUtils.Simulate.click(removeButton)
  expect(removeItem.mock.calls).to.be.lengthOf(1)
  expect(removeItem.mock.calls[0][0]).to.equal(1)

  const moveUpButton = actionButtonsElement.querySelector('.move-up-button')
  ReactTestUtils.Simulate.click(moveUpButton)
  expect(moveUpItem.mock.calls).to.be.lengthOf(1)
  expect(moveUpItem.mock.calls[0][0]).to.equal(1)

  const moveDownButton = actionButtonsElement.querySelector('.move-down-button')
  ReactTestUtils.Simulate.click(moveDownButton)
  expect(moveDownItem.mock.calls).to.be.lengthOf(1)
  expect(moveDownItem.mock.calls[0][0]).to.equal(1)
})

it('should not display action buttons if edit mode is off', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <MuiThemeProvider>
      <ListView items={items} editMode={false} />
    </MuiThemeProvider>,
    div
  )
  const itemElements = div.querySelectorAll('.item')
  itemElements.forEach(itemElement => {
    expect(itemElement.querySelector('.action-buttons')).not.to.exist
  })
})
