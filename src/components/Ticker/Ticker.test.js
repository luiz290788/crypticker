import React from 'react'
import ReactDOM from 'react-dom'
import Ticker from './Ticker'
import { expect } from 'chai'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

it('shows ticker info', () => {
  const currencies = [{ id: 'BTC', symbol: 'Ƀ' }, { id: 'EUR', symbol: '€' }]
  const ticker = {
    id: 'BTCEUR',
    currFrom: 'BTC',
    currTo: 'EUR',
    value: 950.0
  }
  const div = document.createElement('div')
  ReactDOM.render(
    <MuiThemeProvider>
      <Ticker ticker={ticker} currencies={currencies} />
    </MuiThemeProvider>,
    div
  )
  const currencyFromSymbolElement = div.querySelector('.currency-from-symbol')
  expect(currencyFromSymbolElement).to.exist
  expect(currencyFromSymbolElement.innerHTML).to.equal('Ƀ')
  const currencyToSymbolElement = div.querySelector('.currency-to-symbol')
  expect(currencyToSymbolElement).to.exist
  expect(currencyToSymbolElement.innerHTML).to.equal('€')
  const tickerValueElement = div.querySelector('.ticker-value')
  expect(tickerValueElement).to.exist
  expect(tickerValueElement.innerHTML).to.equal('950')
})

it('shows "…" if no value', () => {
  const currencies = [{ id: 'BTC', symbol: 'Ƀ' }, { id: 'EUR', symbol: '€' }]
  const ticker = {
    id: 'BTCEUR',
    currFrom: 'BTC',
    currTo: 'EUR',
    value: null
  }
  const div = document.createElement('div')
  ReactDOM.render(
    <MuiThemeProvider>
      <Ticker ticker={ticker} currencies={currencies} />
    </MuiThemeProvider>,
    div
  )
  const currencyFromSymbolElement = div.querySelector('.currency-from-symbol')
  expect(currencyFromSymbolElement).to.exist
  expect(currencyFromSymbolElement.innerHTML).to.equal('Ƀ')
  const currencyToSymbolElement = div.querySelector('.currency-to-symbol')
  expect(currencyToSymbolElement).to.exist
  expect(currencyToSymbolElement.innerHTML).to.equal('€')
  const tickerValueElement = div.querySelector('.ticker-value')
  expect(tickerValueElement).to.exist
  expect(tickerValueElement.innerHTML).to.equal('…')
})
